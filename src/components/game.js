import React from 'react'
import {timestampToDateTime} from 'helpers/date'
import Player from 'components/player'

class Game extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const {game, count, empty} = this.props;
        return (
            <div id="liveVideo">
                <div className="video">
                    {game.matchId && (
                        <Player game={game} />
                    )}
                    {/*<div className="mask">*/}
                    {/*    <div className="NotStarted">*/}
                    {/*        <div className="status">*/}
                    {/*            <h5>2021-06-03 20:00</h5>*/}
                    {/*            <p>开赛倒数 00:03:00</p>*/}
                    {/*        </div>*/}
                    {/*        <div className="homeTeam">*/}
                    {/*            <img width="70" src="img/homeTeamLogo.png" alt="homeTeamLogo"/>*/}
                    {/*            <h6 className="teamName">队伍名称A12(主)</h6>*/}
                    {/*            <h2 className="score">5</h2>*/}
                    {/*            <ul>*/}
                    {/*                <li className="l">L</li>*/}
                    {/*                <li className="l">L</li>*/}
                    {/*                <li className="w">W</li>*/}
                    {/*                <li className="w">W</li>*/}
                    {/*                <li className="w">W</li>*/}
                    {/*            </ul>*/}
                    {/*        </div>*/}
                    {/*        <div className="vs">:</div>*/}
                    {/*        <div className="awayTeam">*/}
                    {/*            <img width="70" src="img/awayTeamLogo.png" alt="awayTeamLogo"/>*/}
                    {/*            <h6 className="teamName">队伍名称</h6>*/}
                    {/*            <h2 className="score">3</h2>*/}
                    {/*            <ul>*/}
                    {/*                <li className="l">L</li>*/}
                    {/*                <li className="l">L</li>*/}
                    {/*                <li className="w">W</li>*/}
                    {/*                <li className="w">W</li>*/}
                    {/*                <li className="w">W</li>*/}
                    {/*            </ul>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}

                    {/*    <div className="stop" style={{display: "none"}}>*/}
                    {/*        <div className="status">*/}
                    {/*            <h5>完赛</h5>*/}

                    {/*        </div>*/}
                    {/*        <div className="homeTeam">*/}
                    {/*            <img width="70" src="img/homeTeamLogo.png" alt="homeTeamLogo"/>*/}
                    {/*            <h6 className="teamName">队伍名称A12(主)</h6>*/}
                    {/*            <h2 className="score">5</h2>*/}
                    {/*        </div>*/}
                    {/*        <div className="vs">:</div>*/}
                    {/*        <div className="awayTeam">*/}
                    {/*            <img width="70" src="img/awayTeamLogo.png" alt="awayTeamLogo"/>*/}
                    {/*            <h6 className="teamName">队伍名称</h6>*/}
                    {/*            <h2 className="score">3</h2>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}

                    {/*</div>*/}

                </div>
                <div className="videoInfo">
                    <h1>
                        {/*<span className="badge baoBen">保本</span>*/}
                        {empty && (
                            <>
                                【目前無賽事】
                            </>
                        )}
                        {game.matchId == null && !empty && (
                            <>
                                loading . . .
                            </>
                        )}
                        {game.matchId && (
                            <>
                            {timestampToDateTime(game.openDate)} 【{game.leagueName}】
                            <h2>{game.homeName}(主) vs {game.awayName}</h2>
                            </>
                        )}
                    </h1>
                    <span className="watchNum" style={{"grid-area":"time"}}>{count}</span>
                    {/*<span className="time">03:10:00</span>*/}
                    {/*<span className="watchNum">99,520,333</span>*/}

                    {/*<span className="badge live">直播中</span>*/}
                    {/*<h2>队伍名称A12(主) vs 队伍名称</h2>*/}
                    {/*<select name="sources" id="sources">*/}
                    {/*    <option value="1">讯号源 01</option>*/}
                    {/*</select>*/}
                </div>
            </div>
        )
    }
}

export default Game