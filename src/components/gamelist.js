import React from 'react'
import {timestampToMM_dd_hh_mm} from 'helpers/date'

class GameList extends React.Component {
    constructor(props) {
        super(props);
    }
    changeGameIndex(index) {
        this.props.selectGame(this.props.games[index]);
    }
    getCardClass(item){
        let className = "card";
        if(item.matchId == this.props.game.matchId){
            className = "card active";
        }
        return className;
    }
    render() {
        return (
            <div id="matchList">
                <h3>赛事列表</h3>
                {/*<ul className="tabList">*/}
                {/*    <li className="active">全部</li>*/}
                {/*    <li>今日</li>*/}
                {/*    <li>明日</li>*/}
                {/*    <li>直播</li>*/}
                {/*</ul>*/}
                <div className="matchList">
                    {this.props.games.map((item, index) => (
                        <div key={index} className={this.getCardClass(item)} onClick={() => this.changeGameIndex(index)}>
                            <h5 className="cardTitle">
                                <span>{item.leagueName}</span>
                            </h5>
                            <p className="infomation">
                                <span>{timestampToMM_dd_hh_mm(item.openDate)}</span>
                                {/*<span>06-02</span>*/}
                                {/*<span>15:03</span>*/}

                                {/*<span className="baoBen">保本</span>*/}
                                {/*<span className="live">直播中</span>*/}
                            </p>
                            <ul>
                                <li>{item.homeName}(主)</li>
                                <li>{item.awayName}</li>
                            </ul>
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}

export default GameList
