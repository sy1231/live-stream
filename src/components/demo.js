import React from 'react';
import 'dist/css/main.min.css'

function Demo() {
    return (
        <main>
            <div id="matchList">
                <h3>赛事列表</h3>
                <ul className="tabList">
                    <li className="active">全部</li>
                    <li>今日</li>
                    <li>明日</li>
                    <li>直播</li>
                </ul>
                <div className="matchList">
                    <div className="card active">
                        <h5 className="cardTitle">
                            <span>赛季名称赛季名称赛季名称赛季名称</span>
                        </h5>
                        <p className="infomation">
                            <span>06-02</span>
                            <span>15:03</span>
                            <span className="baoBen">保本</span>
                            <span className="live">直播中</span>
                        </p>
                        <ul>
                            <li>地主队名称A12(主)</li>
                            <li>客队名称</li>
                        </ul>
                    </div>
                    <div className="card">
                        <h5 className="cardTitle">
                            赛季名称赛季名称
                        </h5>
                        <p className="infomation">
                            <span>06-02</span>
                            <span>15:03</span>

                            <span className="baoBen">保本</span>
                            <span className="prepareLive">直播</span>
                        </p>
                        <ul>
                            <li>地主队名称A12(主)</li>
                            <li>客队名称</li>
                        </ul>
                    </div>
                    <div className="card">
                        <h5 className="cardTitle">
                            赛季名称赛季名称
                        </h5>
                        <p className="infomation">
                            <span>06-02</span>
                            <span>15:03</span>
                            <span className="baoBen">保本</span>
                            <span className="prepareLive">直播</span>
                        </p>
                        <ul>
                            <li>地主队名称A12(主)</li>
                            <li>客队名称</li>
                        </ul>
                    </div>
                    <div className="card">
                        <h5 className="cardTitle">
                            赛季名称赛季名称
                        </h5>
                        <p className="infomation">
                            <span>06-02</span>
                            <span>15:03</span>

                            <span className="baoBen">保本</span>
                            <span className="prepareLive">直播</span>
                        </p>
                        <ul>
                            <li>地主队名称A12(主)</li>
                            <li>客队名称</li>
                        </ul>
                    </div>
                    <div className="card">
                        <h5 className="cardTitle">
                            赛季名称赛季名称
                        </h5>
                        <p className="infomation">
                            <span>06-02</span>
                            <span>15:03</span>

                            <span className="baoBen">保本</span>
                            <span className="prepareLive">直播</span>
                        </p>
                        <ul>
                            <li>地主队名称A12(主)</li>
                            <li>客队名称</li>
                        </ul>
                    </div>
                    <div className="card">
                        <h5 className="cardTitle">
                            赛季名称赛季名称
                        </h5>
                        <p className="infomation">
                            <span>06-02</span>
                            <span>15:03</span>


                            <span className="baoBen">保本</span>
                            <span className="prepareLive">直播</span>
                        </p>
                        <ul>
                            <li>地主队名称A12(主)</li>
                            <li>客队名称</li>
                        </ul>
                    </div>
                    <div className="card">
                        <h5 className="cardTitle">
                            赛季名称赛季名称
                        </h5>
                        <p className="infomation">
                            <span>06-02</span>
                            <span>15:03</span>

                            <span className="baoBen">保本</span>
                            <span className="prepareLive">直播</span>
                        </p>
                        <ul>
                            <li>地主队名称A12(主)</li>
                            <li>客队名称</li>
                        </ul>
                    </div>
                    <div className="card">
                        <h5 className="cardTitle">
                            赛季名称赛季名称
                        </h5>
                        <p className="infomation">
                            <span>06-02</span>
                            <span>15:03</span>

                            <span className="baoBen">保本</span>
                            <span className="prepareLive">直播</span>
                        </p>
                        <ul>
                            <li>地主队名称A12(主)</li>
                            <li>客队名称</li>
                        </ul>
                    </div>

                </div>

            </div>


            <div id="liveVideo">
                <div className="video">
                    <div className="mark">
                        <span className="badge live">LIVE</span>
                        <a href="" className="closeChat">
                            <svg xmlns="http://www.w3.org/2000/svg" width="17" height="14" viewBox="0 0 17 14">
                                <path d="M11.5,17.5h6v-6h4l-7-7-7,7h4Zm-4,2h14v2H7.5Z"
                                      transform="translate(21.5 -7.5) rotate(90)" />
                            </svg>
                        </a>
                    </div>

                    <video controls>
                        <source src="img/test.mp4" type="video/mp4"/>
                    </video>

                    <div className="mask">

                        <div className="NotStarted">
                            <div className="status">
                                <h5>2021-06-03 20:00</h5>
                                <p>开赛倒数 00:03:00</p>
                            </div>
                            <div className="homeTeam">
                                <img width="70" src="img/homeTeamLogo.png" alt="homeTeamLogo"/>
                                <h6 className="teamName">队伍名称A12(主)</h6>
                                <h2 className="score">5</h2>
                                <ul>
                                    <li className="l">L</li>
                                    <li className="l">L</li>
                                    <li className="w">W</li>
                                    <li className="w">W</li>
                                    <li className="w">W</li>
                                </ul>
                            </div>
                            <div className="vs">:</div>
                            <div className="awayTeam">
                                <img width="70" src="img/awayTeamLogo.png" alt="awayTeamLogo"/>
                                <h6 className="teamName">队伍名称</h6>
                                <h2 className="score">3</h2>
                                <ul>
                                    <li className="l">L</li>
                                    <li className="l">L</li>
                                    <li className="w">W</li>
                                    <li className="w">W</li>
                                    <li className="w">W</li>
                                </ul>
                            </div>

                        </div>

                        <div className="stop" style={{display: "none"}}>
                            <div className="status">
                                <h5>完赛</h5>

                            </div>
                            <div className="homeTeam">
                                <img width="70" src="img/homeTeamLogo.png" alt="homeTeamLogo"/>
                                <h6 className="teamName">队伍名称A12(主)</h6>
                                <h2 className="score">5</h2>
                            </div>
                            <div className="vs">:</div>
                            <div className="awayTeam">
                                <img width="70" src="img/awayTeamLogo.png" alt="awayTeamLogo"/>
                                <h6 className="teamName">队伍名称</h6>
                                <h2 className="score">3</h2>
                            </div>
                        </div>

                    </div>

                </div>
                <div className="videoInfo">
                    <h1><span className="badge baoBen">保本</span> 2021-06-02 03:00 【赛季名称赛季名称】</h1>
                    <span className="time">03:10:00</span>
                    <span className="watchNum">99,520,333</span>

                    <span className="badge live">直播中</span>
                    <h2>队伍名称A12(主) vs 队伍名称</h2>
                    <select name="sources" id="sources">
                        <option value="1">讯号源 01</option>
                    </select>
                </div>
            </div>


            <div id="chatRoom">

                <span className="badge">
                滑到最底
            </span>

                <div className="header">
                    <a href="" className="closeChat">
                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="14" viewBox="0 0 17 14">
                            <path d="M11.5,17.5h6v-6h4l-7-7-7,7h4Zm-4,2h14v2H7.5Z"
                                  transform="translate(21.5 -7.5) rotate(90)"/>
                        </svg>
                    </a>
                    <h3>实况聊天室</h3>
                    <a href="" className="more">
                        <svg xmlns="http://www.w3.org/2000/svg" width="4" height="16" viewBox="0 0 4 16">
                            <path
                                d="M19.187,8.75a2,2,0,1,0-2,2A2.006,2.006,0,0,0,19.187,8.75Zm0,12a2,2,0,1,0-2,2A2.006,2.006,0,0,0,19.187,20.75Zm0-6a2,2,0,1,0-2,2A2.006,2.006,0,0,0,19.187,14.75Z"
                                transform="translate(-15.188 -6.75)"/>
                        </svg>
                    </a>
                </div>

                <div className="talkContent">

                    <p className="note">
                        13:00
                    </p>

                    <div className="other">
                        <img src="img/demoPhoto.png" alt="users" />
                            <p className="name">使用者名称</p>
                            <div className="speak">
                                <p>对话内容对话内容对话内容对话内容对话内容对话内容对话内容</p>
                                <img src="img/test-sticker.png" alt="test-sticker" />
                            </div>
                    </div>

                    <div className="mine">
                        <div className="speak">
                            <p>人之初原本山 AK47没有B</p>
                        </div>
                    </div>

                    <p className="note">
                        13:05
                    </p>
                    <div className="other">
                        <img src="img/demoPhoto-2.png" alt="users"/>
                        <p className="name">使用者名称</p>
                        <div className="speak">
                            <p>我谁</p>
                            <p>北海岸鳕鱼香丝~~~
                            <br /> 万得佛罗里达
                        <br />
                        阿里巴巴把啦爸巴巴
                    </p>
                </div>
            </div>
            <div className="mine">
                <div className="speak">
                    <p>对</p>
                    <img src="img/test-sticker.png" alt="test-sticker"/>
                    <p>对话内容对话内容对话内容对话内容对话内容对话内容对话！ 内容对话内容对话内容对话内容对话内容对话内容对话内容对话内容</p>
                </div>
            </div>
        </div>


    <div className="inputArea">
        <div className="stickerReview">
            <div className="allSticker">
                <img src="img/test-sticker.png" alt=""/>
                <img src="img/test-sticker.png" alt=""/>
                <img src="img/test-sticker.png" alt=""/>
                <img src="img/test-sticker.png" alt=""/>
                <img src="img/test-sticker.png" alt=""/>
                <img src="img/test-sticker.png" alt=""/>
                <img src="img/test-sticker.png" alt=""/>
                <img src="img/test-sticker.png" alt=""/>
            </div>
            <div className="stickerList">
                <button type="button" className="left" disabled>＜</button>
                <button type="button" className="right">＞</button>
                <img src="img/test-sticker.png" alt=""/>
                <img src="img/test-sticker.png" alt=""/>
                <img src="img/test-sticker.png" alt=""/>
                <img src="img/test-sticker.png" alt=""/>
                <img src="img/test-sticker.png" alt=""/>
            </div>
        </div>

        <textarea name="textField" id="textField" cols="25" rows="4" placeholder="输入讯息"></textarea>
        <div className="tools">

            <a href="#" className="emoji">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                     viewBox="0 0 20 20">
                    <path
                        d="M12.99,3A10,10,0,1,0,23,13,10,10,0,0,0,12.99,3ZM13,21a8,8,0,1,1,8-8A8,8,0,0,1,13,21Zm3.5-9A1.5,1.5,0,1,0,15,10.5,1.5,1.5,0,0,0,16.5,12Zm-7,0A1.5,1.5,0,1,0,8,10.5,1.5,1.5,0,0,0,9.5,12ZM13,18.5A5.5,5.5,0,0,0,18.11,15H7.89A5.5,5.5,0,0,0,13,18.5Z"
                        transform="translate(-3 -3)"/>
                </svg>
            </a>

            <a href="#" className="fileUpload">
                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="20"
                     viewBox="0 0 17 20">
                    <path
                        d="M18.818,6.445a2.6,2.6,0,0,1,.531.848,2.594,2.594,0,0,1,.221.982V21.132a1.038,1.038,0,0,1-.31.759,1.02,1.02,0,0,1-.753.313H3.633a1.02,1.02,0,0,1-.753-.313,1.038,1.038,0,0,1-.31-.759V3.275a1.038,1.038,0,0,1,.31-.759A1.02,1.02,0,0,1,3.633,2.2H13.55a2.537,2.537,0,0,1,.974.223,2.573,2.573,0,0,1,.841.536ZM13.9,3.721v4.2h4.161a1.222,1.222,0,0,0-.243-.458L14.358,3.967a1.206,1.206,0,0,0-.454-.246Zm4.25,17.054V9.346h-4.6a1.02,1.02,0,0,1-.753-.312,1.038,1.038,0,0,1-.31-.759V3.632h-8.5V20.775H18.154Zm-1.417-5v3.571H5.4V17.2l2.125-2.143,1.417,1.429L13.2,12.2ZM7.529,13.632a2.041,2.041,0,0,1-1.505-.625,2.168,2.168,0,0,1,0-3.036,2.125,2.125,0,0,1,3.01,0,2.168,2.168,0,0,1,0,3.036A2.041,2.041,0,0,1,7.529,13.632Z"
                        transform="translate(-2.571 -2.203)"/>
                </svg>
            </a>

            <span>0/20</span>

            <a href="#" id="send">
                <svg xmlns="http://www.w3.org/2000/svg" width="20.002" height="20"
                     viewBox="0 0 20.002 20">
                    <path
                        d="M23.874,4.548,4.751,12.883a.438.438,0,0,0,.016.8L9.939,16.6a.834.834,0,0,0,.953-.094l10.2-8.793c.068-.057.229-.167.292-.1s-.036.224-.094.292l-8.825,9.939a.831.831,0,0,0-.083.995l3.381,5.423a.44.44,0,0,0,.792-.01L24.463,5.126A.438.438,0,0,0,23.874,4.548Z"
                        transform="translate(-4.503 -4.503)"/>
                </svg>
            </a>
        </div>
    </div>
</div>
</main>
);
}

export default Demo;
