import React from 'react'

class MsgShow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            msgHight: 0,
            moreMsg: false
        };
        this.scrollIntoView = this.scrollIntoView.bind(this);
    }

    scrollIntoView() {
        this.refs.msgDiv.scrollTop = this.refs.msgDiv.scrollHeight;
        this.setState({
            msgHight: this.refs.msgDiv.scrollHeight
        });
    }

    componentDidUpdate() {
        if (this.refs.msgDiv.scrollHeight == this.refs.msgDiv.clientHeight){
            if (this.state.msgHight != this.refs.msgDiv.scrollHeight){
                this.setState({
                    msgHight: this.refs.msgDiv.scrollHeight,
                });
            }
        }

        const moreMsg = this.refs.msgDiv.scrollHeight > this.state.msgHight;
        if(this.state.moreMsg != moreMsg){
            this.setState({
                moreMsg: moreMsg,
            });
        }
    }

    render() {
        const {msgList, handleClick, user} = this.props;
        let lastTime = "";
        const getTimeView = (createTime) =>{
            if (lastTime != createTime){
                lastTime = createTime;
                return (
                    <p className="note">
                        {lastTime}
                    </p>
                )
            }
            return null;
        }
        return (
            <>
                {this.state.moreMsg && (
                    <span className="badge" onClick={this.scrollIntoView}>滑到最底</span>
                )}
                <div className="header">
                    {/*<a href="" className="closeChat">*/}
                    {/*    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="14" viewBox="0 0 17 14">*/}
                    {/*        <path d="M11.5,17.5h6v-6h4l-7-7-7,7h4Zm-4,2h14v2H7.5Z"*/}
                    {/*              transform="translate(21.5 -7.5) rotate(90)"/>*/}
                    {/*    </svg>*/}
                    {/*</a>*/}
                    <h3>实况聊天室</h3>
                    {/*<a href="" className="more">*/}
                    {/*    <svg xmlns="http://www.w3.org/2000/svg" width="4" height="16" viewBox="0 0 4 16">*/}
                    {/*        <path*/}
                    {/*            d="M19.187,8.75a2,2,0,1,0-2,2A2.006,2.006,0,0,0,19.187,8.75Zm0,12a2,2,0,1,0-2,2A2.006,2.006,0,0,0,19.187,20.75Zm0-6a2,2,0,1,0-2,2A2.006,2.006,0,0,0,19.187,14.75Z"*/}
                    {/*            transform="translate(-15.188 -6.75)"/>*/}
                    {/*    </svg>*/}
                    {/*</a>*/}
                    <a href="" onClick={handleClick} className="closeChat">
                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="14" viewBox="0 0 17 14">
                            <path d="M11.5,17.5h6v-6h4l-7-7-7,7h4Zm-4,2h14v2H7.5Z"
                                  transform="translate(21.5 -7.5) rotate(90)"/>
                        </svg>
                    </a>
                </div>
                <div id="msgDiv" ref="msgDiv" className="talkContent">
                    {msgList.map((item, index) => {
                        if (item.msgText != '') {
                            return (
                                <>
                                    {getTimeView(item.createTime)}
                                    <div {...(item.fromUser.chatUserName == user.username ? { className: "mine" } : { className: "other" })} >
                                        {item.fromUser.chatUserName != user.username && (
                                            <p className="name">{item.fromUser.nickName}</p>
                                        )}
                                        {/*<div className="speak">*/}
                                        {/*    <p>{item.msgText}</p>*/}
                                        {/*</div>*/}
                                        <div className="speak">
                                            <p dangerouslySetInnerHTML={{
                                                __html: item.msgText
                                            }} />
                                        </div>
                                    </div>
                                </>
                            )
                        }
                    })}
                </div>
            </>
        )
    }
}

export default MsgShow