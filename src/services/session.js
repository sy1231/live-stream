export default class SessionService {
    static setToken(data) {
        sessionStorage.setItem("token", data);
    }
    static getToken() {
        return sessionStorage.getItem("token");
    }
    static removeToken() {
        sessionStorage.removeItem("token");
    }
}