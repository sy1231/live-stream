function user_get(user) {
  return {
    type: 'USER_GET',
    user
  }
}

function conn_get(host) {
  return {
    type: 'WS_CONNECT',
    host
  }
}

function message_update(msg) {
  return {
    type: 'MSG_UPDATE',
    msg
  }
}

function message_get(getmsg) {
  return {
    type: 'MSG_GET',
    getmsg
  }
}

function message_clean() {
  return {
    type: 'MSG_CLEAN'
  }
}

function count_update(count) {
  return {
    type: 'COUNT_UPDATE',
    count
  }
}

function games_get(games) {
  return {
    type: 'GAMES_GET',
    games
  }
}

function game_current(game) {
  return {
    type: 'GAME_CURRENT',
    game
  }
}

function games_empty(empty) {
  return {
    type: 'GAMES_EMPTY',
    empty
  }
}

export { message_update, user_get, conn_get, message_get, count_update, games_get, game_current, message_clean, games_empty}
